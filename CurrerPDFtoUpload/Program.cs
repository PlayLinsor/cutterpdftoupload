﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace CurrerPDFtoUpload
{
    class Program
    {
        static void Main(string[] args)
        {
            // Начальные настройки
            long Limit = 9800*1000;
            string PathFile = "1.pdf";
 
            if (args.Length >= 1) PathFile = args[0];
            if (args.Length >= 2) Limit = Convert.ToInt32(args[0]);


            Console.WriteLine("Файл - {0}\nРазбивать по {1} bytes", PathFile, Limit);
            try
            {
                // Окрытие Файла
                FileInfo fi = new FileInfo(PathFile);
                PdfReader reader = new PdfReader(PathFile);

                int idx = Convert.ToInt32(Limit * reader.NumberOfPages / fi.Length);
                int fidx = 0;
                int sidx = 1;
                int eidx = idx;


                while (sidx < reader.NumberOfPages)
                {
                    CreatePdf(string.Format("{0}_{1}.pdf",
                        Path.GetFileNameWithoutExtension(fi.FullName), ++fidx),
                        reader,
                        sidx,
                        eidx);

                    sidx += idx;
                    eidx += idx;
                    if (eidx > reader.NumberOfPages) eidx = reader.NumberOfPages;
                }

                Console.WriteLine("Файл Успешно подготовлен к отправке");
            }
            catch (Exception ex)
            {
                var l = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\nОШИБКА - {0}", ex.Message);
                Console.ForegroundColor = l;
            }
            Console.ReadLine();
        }

        /// <summary>
        /// Создаем Новый ПДФ Файл
        /// </summary>
        /// <param name="PathPdf">Путь к файлу</param>
        /// <param name="MainReader">Откуда брать инфу</param>
        /// <param name="pageStart">Начальная страница</param>
        /// <param name="pageEnd">Конечная страница</param>
        static public void CreatePdf(string PathPdf,PdfReader MainReader,int pageStart,int pageEnd)
        {
            if (pageEnd <= MainReader.NumberOfPages)
            {
                // Создание нового файла
                FileStream fs = File.Create(PathPdf);
                Document document = new Document(MainReader.GetPageSizeWithRotation(1));
                PdfCopy copy = new PdfCopy(document, fs);
                document.Open();

                // Копирование страниц
                PdfImportedPage page;
                for (int i = pageStart; i <= pageEnd; i++)
                {
                    page = copy.GetImportedPage(MainReader, i);
                    copy.AddPage(page);
                }

                copy.Close();
                document.Close();
                fs.Close();

                var l = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Создан файл {0}", PathPdf);
                Console.ForegroundColor = l;
            }
        }
    }
}

